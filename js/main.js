const navbartoggle = document.querySelector('.navbar .toggle');
const sidebar = document.querySelector('.sidebar');
const navbar = document.querySelector('.navbar');
const body = document.querySelector('body');

let sidebarstatus = false;

navbartoggle.addEventListener('click', ()=>{
    if(!sidebarstatus){
        navbartoggle.classList.add('open');
        sidebar.classList.add('open');
        navbar.classList.add('solid');
        body.classList.add('fixed');
        sidebarstatus = true;
    } else {
        navbartoggle.classList.remove('open');
        sidebar.classList.remove('open');
        navbar.classList.remove('solid');
        body.classList.remove('fixed');
        sidebarstatus = false;
    }
});

// window.onscroll = function() {navbarScroll()};

// function navbarScroll(){
//     if(document.body.scrollTop > 75 || document.documentElement.scrollTop > 75){
//         navbar.classList.add('scroll');
//         sidebar.classList.add('scroll');
//     }else{
//         navbar.classList.remove('scroll');
//         sidebar.classList.remove('scroll');
//     }
// }